﻿
Public Module Connection

    ''' <summary>
    ''' Format a MySQL Connection String
    ''' </summary>
    ''' <param name="Server">Server</param>
    ''' <param name="Port">Server Port</param>
    ''' <param name="UserID">User ID</param>
    ''' <param name="Password">Password</param>
    ''' <param name="CmdTimeout">Command Timeout</param>
    ''' <param name="ConnectionTimeout">Connetion Timeout</param>
    ''' <returns>Connection String</returns>
    ''' <search>interoperability, database, slingshot, sql, connection</search>
    Public Function MySQL_ConnectionString(Server As String,
                                     Port As String,
                                     UserID As String,
                                     Password As String,
                                     CmdTimeout As String,
                                     ConnectionTimeout As String) As String
        Try
            Dim connectionstring As String = "Server=" & Server & "; Port=" & Port & "; Uid=" & UserID & "; Pwd=" & Password & "; default command timeout=" & CmdTimeout & "; Connection Timeout=" & ConnectionTimeout
            Return connectionstring
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Module
