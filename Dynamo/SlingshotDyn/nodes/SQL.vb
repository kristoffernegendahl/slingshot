﻿Imports System
Imports System.IO
Imports System.Data

Public Module SQL

#Region "Node Functions"
    ''' <summary>
    ''' SQL INSERT INTO String Constructor
    ''' </summary>
    ''' <param name="Table">Table to insert into</param>
    ''' <param name="Columns">List of Columns</param>
    ''' <param name="Values">Values to insert</param>
    ''' <returns>SQL String</returns>
    ''' <search>interoperability, database, slingshot, sql, insert</search>
    Public Function InsertInto(Table As String,
                               Columns As List(Of String),
                               Values As Object()()) As List(Of String)
        Try
            'create a column string
            Dim m_columnstring As String = Columns(0)
            If Columns.Count > 0 Then
                For i As Integer = 1 To Columns.Count - 1
                    m_columnstring = m_columnstring & "," & Columns(i)
                Next
            End If

            Dim m_cmd As String
            Dim m_cmdlist As New List(Of String)

            Dim m_rowscount As Integer = Values.Length
            Dim m_colscount As Integer = Values(0).Length

            For i As Integer = 0 To m_rowscount - 1

                ' Combine values
                Dim vals As Object() = Values(i)
                Dim m_valstring As String = "'" & Convert.ToString(vals(0)) & "'"
                For j As Integer = 1 To m_colscount - 1
                    m_valstring = m_valstring & "," & "'" & Convert.ToString(vals(j)) & "'"
                Next

                ' Create command string
                m_cmd = "INSERT INTO " & Table & "(" & m_columnstring & ") VALUES (" & m_valstring & ")"
                m_cmdlist.Add(m_cmd)
            Next

            Return m_cmdlist
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' SQL SELECT FROM String Constructor
    ''' </summary>
    ''' <param name="SelectItem">Select items</param>
    ''' <param name="FromTable">From Table</param>
    ''' <returns>SQL String</returns>
    ''' <search>interoperability, database, slingshot, sql, select</search>
    Public Function SelectFrom(SelectItem As String,
                               FromTable As String) As String
        Try
            Dim m_query As String = "SELECT " & SelectItem & " FROM " & FromTable
            Return m_query
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' SQL SELECT FROM WHERE String Constructor
    ''' </summary>
    ''' <param name="SelectItem">Select items</param>
    ''' <param name="FromTable">From Table</param>
    ''' <param name="WhereCondition"> Where Condition</param>
    ''' <returns>SQL String</returns>
    ''' <search>interoperability, database, slingshot, sql, select</search>
    Public Function SelectFromWhere(SelectItem As String,
                               FromTable As String,
                               WhereCondition As String) As String
        Try
            Dim m_query As String = "SELECT " & SelectItem & " FROM " & FromTable & " WHERE " & WhereCondition
            Return m_query
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

End Module
